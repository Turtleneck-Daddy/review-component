import React, { Component } from 'react';
import './App.css';

class ProductDetails extends Component {
    render() {
        return(
            <section className="PictureNDescription">    
                <div className="Picture">
                    <img  alt="" src={this.props.productpic} />
                </div>

                <div className="Description">
                    <h3>{this.props.title}</h3>
                    <p>{this.props.description}</p>
                    
                    <a href={this.props.link}>Check it out</a>
                </div>          
            </section>
        );
    }
}

export default ProductDetails;