import React, { Component } from 'react';
import './App.css';


class Review extends Component {
    render() {
        return(
            <section className="Reviews">
                <div className="Review">
                    <div className="QuoteAuthorPic">
                        <img alt={this.props.authorname} src={this.props.authorpic} />
                    </div>
                    <blockquote className="Quote" >"{this.props.reviewquote}"</blockquote>
                    <div className="QuoteAuthor">
                        <p>{this.props.authorname}</p>
                    </div>
                </div>
            </section>
        );
    }

}

export default Review;