import React, { Component } from 'react';
import './App.css';


class Review2 extends Component {
    render() {
        return(
            <section className="Reviews">
                <blockquote className="Quote" >"{this.props.reviewquote}"</blockquote>
                <div className="Review">
                    <div className="QuoteAuthorPic2">
                        <img alt={this.props.authorname} src={this.props.authorpic} />
                    </div>
                    <div className="QuoteAuthor2">
                        <p>{this.props.authorname}</p>
                    </div>
                </div>
            </section>
        );
    }

}

export default Review2;